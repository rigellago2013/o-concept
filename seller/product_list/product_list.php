<div id="product-table" class="product_container">
  <div class="product_container__search_container">
    Products
    <br/>
    <br/>
    <input class="search product_container__search__input" placeholder="Search" />
    <a href="?mod=product_create" class="product_container__search__search sort">      
      New
    </a>
  </div>
  <div class="product_container__table">
    <table>
      <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Category</th>
        <th>Unit Price</th>
        <th>Rating</th>
        <th>Supplier</th>
        <th>Action</th>
      </thead>
      <tbody class="list" id="table-container">
      </tbody>
    </table>
  </div>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
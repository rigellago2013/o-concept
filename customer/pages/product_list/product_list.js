 // const  id = localStorage.getItem('id');
 // const  descriptionInput = document.getElementById('description');

 // $(document).ready(function(){
 //                var url = `${env.url}/app/api/bidding/getuserbidding.php?id=${id}`;
                
 //                $.getJSON(url,function(data){
 //                    $.each(data.data,function(index,value){
 //                        $('#bidding').append(`<tr><th scope="row">${value.description}</th><td>${value.date}</td><td><span class="badge badge-danger">${value.status}<span></td></tr>`);
 //                    });
 //                    console.log(data)
 //                });
 //        });

// async function addDescription(){
//     const create = (description) =>{

//     }
// } 
const productContainer = document.getElementById('product_container');
/**
 * loadProducts
 * @description Load Product Items
 */
async function loadProducts() {

    //this is not safe
   try {

  const id = localStorage.getItem('id');
  const response = await axios.get(`${env.url}/app/api/bidding/getuserbidding.php?id=${id}`);
  console.log(response.data);
    productContainer.innerHTML = '<div style="text-align: center; margin-top: 20%; grid-column-start:2">Loading...</div>';
    const response = await axios.get(`/app/api/products/read.php`);
    const { data } = response.data;
    productContainer.innerHTML = '';
    
    if (data && data.length <= 0) {
      productContainer.innerHTML = '<div>No Product Available</div>';
    }
    data.forEach((product, value) => {
      productContainer.innerHTML += `
    <div class="content__container__main__products__item" onclick="window.location='?mod=product&id=${
        product.id
        }' " data-product="1"> 
            <div class="content__container__main__products__item__image">
                <img src="${product.image === '' ? '../../assets/250.png' : product.image }" alt="${product.name}" />
                <div class="content__container__main__products__item__price-circle">
                    <div class="content__container__main__products__item__price">₱${
        product.unit_cost
        }</div>
                </div>
            </div>
            <div class="content__container__main__products__item__name">${product.name}</div>
        </div>
    `;
    });
  }catch(e){
    console.error(e)
    productContainer.innerHTML = '<div style="text-align: center; margin-top: 20%; grid-column-start:2">No Bidding A Available</div>';
  }
}



function main() {
  userValidator();
  loadProducts();
}
main();



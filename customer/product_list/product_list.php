<div class="content__container__main">
  <div class="content__container__main__header">Products List</div>
    <div class="content__container__main__products" id="product_container">

      <!--Bidding Div-->
               <!--  <div class="button_bidding">
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalBidding">
                        Post Bidding
                        &nbsp;
                    </button>
                        <br/>
                    <div class="container" style="padding-top:20px; padding-right: 120px;">
                        <table class="table">
                          <thead class="thead-dark" style="background-color: #eeee">
                            <tr>
                              <th scope="col">Description</th>
                              <th scope="col">Date</th>
                              <th scope="col">Status</th>
                            </tr>
                          </thead> -->
                        <!--   <tbody id=bidding> -->
                           <!--  <tr>
                              <th scope="row">1</th>
                              <td>Mark</td>
                              <td>Otto</td> 
                              <td><span class="badge badge-pill badge-success">Success</span></td>
                            </tr> -->
                       <!--    </tbody>
                        </table>
                    </div> -->
        <!--End of the Bidding-->            
          
        <!-- Item Template -->

        <div class="content__container__main__products__item" onclick="window.location='?mod=product&id=1' " data-product="1"> 
            <div class="content__container__main__products__item__image">
                <img src="https://picsum.photos/330/250/?random" alt="product image" />
                <div class="content__container__main__products__item__price-circle">
                    <div class="content__container__main__products__item__price">₱ 50</div>
                </div>
            </div>
            <div class="content__container__main__products__item__name">Sample Item</div>
        </div>
        <!-- Item Ends Here -->
        
        </div>
      </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalBidding" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Create Bidding</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
          <div class="modal-body">
                <textarea class="form-control" id="description" aria-label="With textarea"></textarea>
          </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-success" onclick="alert()">Save changes</button>
      </div>
    </div>
  </div>
</div>


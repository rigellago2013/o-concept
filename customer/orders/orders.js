const URL_ID = new URLSearchParams(window.location.search);
const container = document.getElementById('orderContainer');
const detailcont = document.getElementById('detailcont');;

async function loadOrders() {
  try {
    container.innerHTML = '<div class="error"> Loading... </div>'
    const response = await axios.get(`/app/api/orders/getcustomerorder.php`);
    const data = response.data.data;

    console.log(data)
    container.innerHTML = `<div class="content__container__main__header">Orders</div>
    <div class="content__container__main__order">
       Order No: <ul> <li> <a href="mod=order&id=${data.order.order_id}"> ${data.order.order_num} </a> </li> </ul>
       Total Amount: ${data.order.total_amount} <br>
       Order Date: ${data.order.order_date} <br>
    </div>`;

    for (var j = 0; j < data.order_details.length; j++){
        detailcont.innerHTML += `
        <div class="content__container__main__order">
            <ul> <li> <a href="mod=order&id=${data.order_details[j].detail_id}"> ${data.order_details[j].product_name}</a> </li> </ul>
           <img src="${data.order_details[j].image}" >  <br>
           quantity: ${data.order_details[j].quantity} <br>
           unit cost:  ${data.order_details[j].unit_cost} <br>
           total cost: ${data.order_details[j].total_cost} <br>
        </div>`;

    }

  } catch (error) {
    console.error(error);
    container.innerHTML = '<div class="error"> No Product Found </div>'
  }
}

function main() {
  userValidator();
  loadOrders();
}
main();

<?php
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        header("Access-Control-Allow-Methods: POST");
        header("Access-Control-Max-Age: 3600");
        header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

        include_once '../../../library/database.php';
        include_once '../../models/Products.php';

        $database = new Database();
        $db = $database->getConnection();
        $product = new Product($db);
        $data = json_decode(file_get_contents("php://input"));

        if(
            !empty($data->name) &&
            !empty($data->description) &&
            !empty($data->type_id) &&
            !empty($data->cost) &&
            !empty($data->quantity) &&
            !empty($data->reordering_point)
        ){
            $code = rand ( 10000000 , 99999999 );
            $image_no = rand ( 10000 , 99999 );
            $path = "../../../app/images/".$image_no.".jpeg";
            $image = file_put_contents( $path, base64_decode($data->image) );
            $imagepath = "/app/images/".$image_no.".jpeg";

            $product->name = $data->name;
            $product->description = $data->description;
            $product->type_id = $data->type_id;
            $product->cost = $data->cost;
            $product->quantity = $data->quantity;
            $product->reordering_point = $data->reordering_point;
            $product->date_added = date('Y-m-d H:i:s');
            $product->image = $imagepath;
            $product->code = $code;

            if( $product->create() ) {

                http_response_code(200);
                echo json_encode(array("message" => "Product was created.", "status" => 200));

            } else{

                http_response_code(503);
                echo json_encode(array("message" => "Unable to create product.", "status" => 503));

            }
        } else{

            http_response_code(400);
            echo json_encode(array("message" => "Unable to create product. Data is incomplete."));
        }

?>

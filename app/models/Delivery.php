    <?php
class Delivery {

    private $db;

    public $id, $trackingNumber, $orderId, $description, $status, $courierid;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getDelivery($id) {

        $query = "SELECT d.id as delivery_id, d.tracking_number, d.order_id, d.description, d.status, c.name FROM delivery d LEFT JOIN courier c ON c.id = d.id WHERE order_id = $id ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $res = $stmt->fetchAll(PDO::FETCH_OBJ);

        $data = [];
        foreach ($res as $value) {

            $data = [
                    'delivery_id' => $value->delivery_id,
                    'tracking_number' => $value->tracking_number,
                    'order_id' =>   $value->order_id,
                    'delivery_description' => $value->description,
                    'status' => $value->status,
                    'courier' => $value->name
            ];

        }

        return $data;

    }


    public function addDelivery($orderDetailsId)
    {
        $query = "INSERT INTO cart (tracking_number, order_id, description, status, courier_id) VALUES (:trackingnumber, :orderid, :description, 'ongoing', :courierid) ";
        $stmt = $this->conn->prepare($query);

        $stmt->bindParam(":trackingnumber", $this->trackingNumber);
        $stmt->bindParam(":orderid", $this->orderId);
        $stmt->bindParam(":description", $this->description);
        $stmt->bindParam(":courierid", $this->courierid);

        if( $stmt->execute() ){

            return true;

        }
        return false;
    }




}

?>

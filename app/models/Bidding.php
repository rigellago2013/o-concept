<?php
class Bidding {

    private $conn;

    public $id, $productid, $user_id, $startingamount,$date, $status, $description;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function postBid()
    {
            $query = "INSERT INTO bidding
                        (description, user_id, date)
                      VALUES
                        (:description, :userid, NOW()) ";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(":userid",$this->user_id);
            $stmt->bindParam(":description", $this->description);

            if($stmt->execute()){
                return true;
            } else {
                return false;
            }
    }

    public function addcustomerbid($bidId, $amount)
    {
        $query = "INSERT INTO customer_bids
                        (bidding_id, amount, user_id)
                  VALUES
                        (:biddingid, :amount, :userid) ";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(":biddingid", $bidId);
        $stmt->bindParam(":amount", $amount);
        $stmt->bindParam(":userid", $_SESSION['user_id']);

        if($stmt->execute()){

               return true;

        }

            return false;
    }

    public function closeBid($id)
    {
            $query = "UPDATE
                        bidding
                    SET
                        status = 'closed',
                    WHERE
                        id = :id";

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $id);

            if( $stmt->execute() ) {
                return true;
            } else {

                return false;
            }

    }

    public function getCustomerBids($id)
    {
        $query = "SELECT * FROM bidding WHERE user_id = $id";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $bids = $stmt->fetchAll(PDO::FETCH_OBJ);

            $i=0;
            $data = [];

            foreach ($bids as $value) {
                $data[$i++] = [
                     'id' => $value->id,
                     'user_id' => $value->user_id,
                     'description' => $value->description,
                     'date' => $value->date,
                     'status' => $value->status
                ];
            }

        return $data;

    }


}

?>
